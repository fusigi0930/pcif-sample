#ifndef __PCIF_H__
#define __PCIF_H__

#define ERR_NOERROR                                     0
#define ERR_LINKLOST                                    -1
#define ERR_CONNFAIL                                    -2
#define ERR_CNTFULL                                     -3
#define ERR_BUF_NOT_ENOUGH                              -4
#define ERR_UNKNOW                                      -98
#define ERR_INTERNAL_ERR                                -99

#include <stdint.h>

typedef int (*fnInit)();
typedef void (*fnDeinit)();
typedef int (*fnGetBurnCode)(char*, int, char*, int, uint32_t*, uint32_t*, uint32_t*);
typedef int (*fnGetFunc)();

#endif