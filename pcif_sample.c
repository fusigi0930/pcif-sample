﻿#include <stdio.h>
#include <windows.h>
#include "pcif.h"

fnInit pcifInit;
fnDeinit pcifDeinit;
fnGetBurnCode pcifGetBurnCode;
fnGetFunc pcifGetCurrentCount;
fnGetFunc pcifGetMax;

void printbin(char* buf, int len) {
	if (len == 0) {
		return;
	}
	printf("=================================\nlength: %d", len);
	for (int i = 0; i < len; i++) {
		if (i % 16 == 0) printf("\n");
		printf("0x%x ", buf[i]);
	}
	printf("\n=================================\n");
}

int main() {
	HMODULE hLib = LoadLibrary(TEXT("pcif.dll"));
	if (NULL == hLib) {
		printf("open dll fail, %d\n", GetLastError());
		return 1;
	}

	pcifInit = (fnInit) GetProcAddress(hLib, "Init");
	pcifDeinit = (fnDeinit)GetProcAddress(hLib, "DeInit");
	pcifGetBurnCode = (fnGetBurnCode)GetProcAddress(hLib, "GetBurnCode");
	pcifGetCurrentCount = (fnGetFunc)GetProcAddress(hLib, "GetCurrentCount");
	pcifGetMax = (fnGetFunc)GetProcAddress(hLib, "GetMax");

	if (!pcifInit || !pcifDeinit || !pcifGetBurnCode || !pcifGetCurrentCount || !pcifGetMax) {
		FreeLibrary(hLib);
		return 2;
	}

	char *ci1 = "AAA0E9Km2Rx/1wxGOeOfmJLviq..1053ACEC39";
	char *ci2 = "BB10TEMgzRoVjB+K0B7Fltj1Gq..978FC97039";
	char *ci3 = "CCA0DZPrjz9ilWbKox/Fyxiq2a..BC84BB2239";
	char* ci4 = "DDA0DZPrjz9ilWbKox/Fyxiq2a..BC84BB22";

	char out[256] = { 0 };
	int retCode;
	// Init
	if (ERR_NOERROR != (retCode = pcifInit())) {
		printf("init failed: %d\n", retCode);
		FreeLibrary(hLib);
		return 3;
	}

	int current_count = pcifGetCurrentCount();
	int max = pcifGetMax();
	printf("max: %d, current: %d\n", max, current_count);

	printf("argument1: %s\n", ci1);
	uint32_t out_size, bccnt, bcmax;
	if (ERR_NOERROR != (retCode = pcifGetBurnCode(ci1, strlen(ci1), out, sizeof(out), &out_size, &bccnt, &bcmax))) {
		printf("get burn code failed: %d\n", retCode);
		FreeLibrary(hLib);
		return 4;
	}
	printf("ret code: %d\n", retCode);
	printbin(out, out_size);
	printf("out buffer length: %d, count: %d, max: %d\n", out_size, bccnt, bcmax);

	if (ERR_NOERROR != (retCode = pcifGetBurnCode(ci2, strlen(ci2), out, sizeof(out), &out_size, &bccnt, &bcmax))) {
		printf("get burn code failed: %d\n", retCode);
		FreeLibrary(hLib);
		return 4;
	}
	printf("ret code: %d\n", retCode);
	printbin(out, out_size);
	printf("out buffer length: %d, count: %d, max: %d\n", out_size, bccnt, bcmax);

	if (ERR_NOERROR == (retCode = pcifGetBurnCode(ci3, strlen(ci3), out, sizeof(out), &out_size, &bccnt, &bcmax))) {
		printf("no impassible\n");
		FreeLibrary(hLib);
		return 0;
	}
	printf("ret code: %d\n", retCode);

	if (ERR_NOERROR == (retCode = pcifGetBurnCode(ci4, strlen(ci4), out, sizeof(out), &out_size, &bccnt, &bcmax))) {
		printf("no impassible\n");
		FreeLibrary(hLib);
		return 0;
	}
	printf("ret code: %d\n", retCode);

	pcifDeinit();
	Sleep(1000);
	FreeLibrary(hLib);

	return 0;
}
